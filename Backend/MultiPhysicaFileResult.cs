using Microsoft.AspNetCore.Mvc;
namespace Backend;

public class MultiPhysicalFileActionResult(List<PhysicalFileResult> fileResults) : IActionResult
{
    public async Task ExecuteResultAsync(ActionContext context)
    {
        foreach (var fileResult in fileResults)
        {
            Console.WriteLine("Started downloading " + fileResult.FileName + " " + DateTime.Now);
            await fileResult.ExecuteResultAsync(context);
            Console.WriteLine("Finished downloading " + fileResult.FileName + " " + DateTime.Now);
            //todo ws updates?
        }
    }
}