using Microsoft.AspNetCore.SignalR;
namespace Backend;

public class ServerHub : Hub
{
    public override async Task OnConnectedAsync()
    {
        Console.WriteLine("Connected");
        await base.OnConnectedAsync();
    }

    public void ConnectionCheck()
    {
        
    }

    public override async Task OnDisconnectedAsync(Exception? exception)
    {
        Console.WriteLine("Disconnected " + exception?.Message);
        await base.OnDisconnectedAsync(exception);
    }
}