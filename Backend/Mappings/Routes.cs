using Backend.Endpoints;

namespace Backend.Mappings;

public static class Routes
{
    public static void UseEndpoints(WebApplication app)
    {
        ConfigureEndpoints(app, _ => {});
    }
    
    public static void UseEndpoints(WebApplication app, Action<RouteHandlerBuilder> configuration)
    {
        ConfigureEndpoints(app, configuration);
    }

    private static void ConfigureEndpoints(WebApplication app, Action<RouteHandlerBuilder> configuration)
    {
        var endpoints = typeof(IEndpoint).Assembly
            .DefinedTypes.Where(x =>
                x is { IsAbstract: false, IsInterface: false } && x.IsAssignableTo(typeof(IEndpoint)));
        
        foreach (var endpoint in endpoints)
        {
            var builder = (RouteHandlerBuilder?)endpoint.GetMethod(nameof(IEndpoint.Define))?
                .Invoke(null, [app]);
            
            if (builder != null) 
                configuration(builder);
        }
    }
}