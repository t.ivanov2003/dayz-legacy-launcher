using System.Text.Json;
using Backend.Mappings;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SignalR.Client;

namespace Backend;

public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddAuthorization();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddResponseCompression(opts =>
        {
            opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                new[] { "application/octet-stream" });
        });

        ConfigureAppSettings(builder);        
        builder.Services.AddSignalR(o =>
        {
            o.EnableDetailedErrors = true;
        })
        .AddHubOptions<ServerHub>(hub =>
        {
            hub.EnableDetailedErrors = true;
        });

        const long maxBodySize = 52428800 / 2; // 25 MB
        builder.WebHost.ConfigureKestrel((context, options) =>
        {
            options.Limits.MaxRequestBodySize = maxBodySize;
        });
        
        builder.Services.Configure<FormOptions>(options =>
        {
            options.MultipartBodyLengthLimit = maxBodySize;
        });

        builder.Services.AddSingleton(new HubConnectionBuilder()
            .WithUrl("https://localhost:5400/hub"/*builder.Configuration[nameof(AppSettings.HubHost)] ?? "/hub"*/)
            .WithAutomaticReconnect()
            .Build());

        var app = builder.Build();
        var configuration = app.Configuration[nameof(AppSettings.HttpClientsHost)];
        if (configuration is not null)
        {
            app.Urls.Clear();
            app.Urls.Add(configuration);
        }
        
        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapHub<ServerHub>("/hub"/*app.Configuration[nameof(AppSettings.HubHost)] ?? "/hub"*/);
        app.UseResponseCompression();
	    app.UseWebSockets();
        Routes.UseEndpoints(app);
        app.Run();
    }

    static void ConfigureAppSettings(WebApplicationBuilder builder)
    {
        var fileToDeserialize = "backendAppSettings.json";
        if (File.Exists(fileToDeserialize))
        {
            builder.Configuration.AddJsonFile(fileToDeserialize);
        }
        else
        {
            File.Create(fileToDeserialize);
            File.WriteAllText(fileToDeserialize, JsonSerializer.Serialize(new AppSettings()
            {
                HubHost = "/hub"
            }));
        }
    }
    
    class AppSettings
    {
        public string HttpClientsHost { get; init; } = "";
        public string HubHost { get; init; } = "";
    }
}