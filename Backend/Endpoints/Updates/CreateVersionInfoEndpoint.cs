using Database;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Shared;

namespace Backend.Endpoints.Updates;

public class CreateVersionInfoEndpoint : IEndpoint
{
    private const string Route = "/createVersionInfo";
    private const string CollectionName = "Versions";
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, Execute);
    }

    private static void Execute([FromBody] string versionInfoSerialized)
    {
        VersionInfo? versionInfo = JsonConvert.DeserializeObject<VersionInfo>(versionInfoSerialized);
        if (versionInfo is null)
        {
            return;
        }
        DatabaseService.Create(CollectionName, $"$.{nameof(versionInfo.VersionNumber)}", new DBVersionInfo()
        {
            VersionNumber = versionInfo.VersionNumber,
            SerializedFileHashes = JsonConvert.SerializeObject(versionInfo.FileHashes)
        });
    }

    public class DBVersionInfo
    {
        public VersionNumber VersionNumber { get; set; } = new();
        public string SerializedFileHashes { get; set; } = "";
    }
}