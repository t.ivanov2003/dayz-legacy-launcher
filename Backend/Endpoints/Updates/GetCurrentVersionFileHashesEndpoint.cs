using Database;
using Newtonsoft.Json;
using Shared;

namespace Backend.Endpoints.Updates;

public class GetCurrentVersionFileHashesEndpoint : IEndpoint
{
    private const string Route = "/hashes";
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapGet(Route, Execute);
    }

    private static string Execute()
    {
        string root = Path.Join(Directory.GetCurrentDirectory(), "UploadedFiles"); //todo export
        bool rootEmpty = !Path.Exists(root) || 
                         Directory.GetFiles(root, "*.*", SearchOption.AllDirectories).Length == 0;
        
        VersionInfo versionInfo = new VersionInfo()
        {
            VersionNumber = new VersionNumber()
            {
                Major = 0,
                Minor = 1,
                Patch = 0
            },
            FileHashes = rootEmpty ? [] : FilesUtility.CalculateHashes(root)
        };
        
        return JsonConvert.SerializeObject(versionInfo);
    }
}