using Database;
using Shared;
namespace Backend.Endpoints.Updates;

public class GetFileUpdateInfoEndpoint : IEndpoint
{
    private static readonly string Route = "/patchFiles";
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, ExecuteAsync);
    }

    private static Result<FileUpdateInfo> ExecuteAsync(Dictionary<string, FileHashInfo> clientHashes)
    {
        var latestUpdateInfo = DatabaseService.Read<VersionInfo>("Versions") ?? new VersionInfo(); 
        //todo magic strings suck
        
        if (clientHashes.Count == 0)
        {
            return new Result<FileUpdateInfo>(new Exception("Client file hashes are empty"));
        }

        if (latestUpdateInfo.FileHashes.Count == 0)
        {
            return new Result<FileUpdateInfo>(new Exception("Server file hashes are empty"));
        }

        var missingHashKeys = latestUpdateInfo.FileHashes.Keys.Except(clientHashes.Keys)
            .ToArray();

        var clientHashKeysToDelete = clientHashes.Keys.Except(latestUpdateInfo.FileHashes.Keys)
            .ToList();

        foreach (var key in clientHashKeysToDelete)
        {
            clientHashes.Remove(key);
        }

        var clientHashKeysToCheck = (missingHashKeys.Length == 0 ? 
                latestUpdateInfo.FileHashes.Keys : latestUpdateInfo.FileHashes.Keys.Except(missingHashKeys))
            .ToList();

        var modifiedClientHashKeys = clientHashes
            .Where(h =>
                clientHashKeysToCheck.Contains(h.Key) && latestUpdateInfo.FileHashes[h.Key] != h.Value)
            .Select(h => h.Key)
            .ToArray();

        List<string> filesToDownload = [];
        if (modifiedClientHashKeys.Length > 0)
        {
            filesToDownload.AddRange(modifiedClientHashKeys);
        }

        if (missingHashKeys.Length > 0)
        {
            filesToDownload.AddRange(missingHashKeys);
        }
        
        clientHashKeysToDelete.AddRange(modifiedClientHashKeys);
        return new Result<FileUpdateInfo>(new FileUpdateInfo()
        {
           FilesToDelete = clientHashKeysToDelete,
           FilesToDownload = filesToDownload
        });
    }
}