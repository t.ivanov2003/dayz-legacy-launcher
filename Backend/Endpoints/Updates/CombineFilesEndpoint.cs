using Database;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Shared;

namespace Backend.Endpoints.Updates;

public class CombineFilesEndpoint : IEndpoint
{
    private const string Route = "/combineFiles";
    private static readonly string Root = Path.Combine(Directory.GetCurrentDirectory(), "UploadedFiles");
    
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapGet(Route, Execute);
    }

    private static IActionResult Execute()
    {
        IEnumerable<string> files = Directory.GetFiles(Root, "*.*", SearchOption.AllDirectories)
            .Where(f => f.Contains(".part", StringComparison.InvariantCultureIgnoreCase));
        
        Dictionary<string, string[]> filesChunks = files.GroupBy(f =>
        {
            string fileName = Path.GetFileName(f);
            int extensionStartIndex = fileName.IndexOf(".part", StringComparison.InvariantCultureIgnoreCase);
            return fileName[..extensionStartIndex];
        })
        .ToDictionary(gr => gr.Key, gr => gr.ToArray());

        if (filesChunks.Count > 1)
        {
            foreach (var kvp in filesChunks)
            {
                CombineFiles(kvp.Value, Path.Join(Root, kvp.Key));
            }
        }

        return new OkResult();
    }
    
    private static void CombineFiles(IEnumerable<string> chunkPaths, string destinationFilePath)
    {
        using FileStream destinationStream = new FileStream(destinationFilePath, FileMode.Create, FileAccess.Write);
        foreach (string chunkPath in chunkPaths)
        {
            FileStream chunkStream = new FileStream(chunkPath, FileMode.Open, FileAccess.Read);
            chunkStream.CopyTo(destinationStream);

            chunkStream.Dispose();
            try
            {
                File.Delete(chunkPath);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Could not delete " + chunkPath + " " + ex.Message);
            }
        }
    }
}