using System.IO.Compression;
using Microsoft.AspNetCore.Mvc;
namespace Backend.Endpoints.Updates;

public class DownloadFilesEndpoint : IEndpoint
{
    private const string Route = "/downloadUpdate";
    
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, Execute);
    }
    
    private static IActionResult Execute(IEnumerable<string> filesToDownload)
    {
        using var memoryStream = new MemoryStream();
        using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
        {
            foreach (var file in filesToDownload)
            {
                AddFileToZip(archive, file);
            }
        }

        memoryStream.Position = 0;
        return new FileStreamResult(memoryStream, "application/zip")
        {
            FileDownloadName = "update.zip"
        };
    }

    private static void AddFileToZip(ZipArchive archive, string filePath)
    {
        if (File.Exists(filePath))
        {
            var entry = archive.CreateEntry(Path.GetFileName(filePath));
            using var entryStream = entry.Open();
            using var fileStream = new FileStream(filePath, FileMode.Open);
            fileStream.CopyTo(entryStream);
        }
    }
}