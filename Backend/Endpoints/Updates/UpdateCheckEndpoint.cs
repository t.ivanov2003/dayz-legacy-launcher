using Database;
using Shared;
namespace Backend.Endpoints.Updates;

public class UpdateCheckEndpoint : IEndpoint
{
    private static readonly string Route = "/updateCheck";

    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, ExecuteAsync);
    }

    private static Result<bool> ExecuteAsync(Dictionary<string, FileHashInfo> clientHashKeys)
    {
        var latestUpdateInfo = DatabaseService.Read<VersionInfo>("Versions") ?? new VersionInfo(); 
        //todo magic strings suck
        
        if (clientHashKeys.Count == 0)
        {
            return new Result<bool>(new Exception("Client file hashes are empty"));
        }

        if (latestUpdateInfo.FileHashes.Count == 0)
        {
            return new Result<bool>(new Exception("Server file hashes are empty"));
        }

        if (clientHashKeys.Count != latestUpdateInfo.FileHashes.Count)
        {
            return new Result<bool>(true);
        }

        var clientHashes = clientHashKeys.Values;
        return new Result<bool>(latestUpdateInfo.FileHashes.Values.Any(v => !clientHashes.Contains(v)));
    }
}