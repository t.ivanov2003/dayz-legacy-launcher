using Database;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Shared;

namespace Backend.Endpoints.Updates;

public class UploadFilesEndpoint : IEndpoint
{
    private const string Route = "/uploadFiles";
    private static readonly string Root = Path.Combine(Directory.GetCurrentDirectory(), "UploadedFiles");
    
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, Execute)
            .DisableAntiforgery();
    }

    private static async Task<IActionResult> Execute(IFormFileCollection files)
    {
        if (!Path.Exists(Root))
        {
            Directory.CreateDirectory(Root);
        }
        
        foreach (var file in files)
        {
            if (file.Length > 0)
            {
                string filePath = Path.Join(Root, file.FileName);
                string directoryPath = filePath.Replace(Path.GetFileName(filePath), "");
                if (!Path.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                await using var stream = new FileStream(filePath, FileMode.Create);
                await file.CopyToAsync(stream);
            }
        }
        
        return new OkResult();
    }
}