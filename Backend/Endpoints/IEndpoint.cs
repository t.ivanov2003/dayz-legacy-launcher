namespace Backend.Endpoints;

public interface IEndpoint
{
    static abstract RouteHandlerBuilder Define(IEndpointRouteBuilder app);
}