using System.Text.Json;

namespace Backend.Endpoints.LauncherSettings;

public class GetSettingsEndpoint : IEndpoint
{
    private static readonly string Route = "/launcher-settings";
    
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapGet(Route, Execute);
    }

    private static Shared.LauncherSettings Execute()
    {
        const string settingsFileLocation = "settings.json";
        var settings = new Shared.LauncherSettings();

        if (File.Exists(settingsFileLocation))
        {
            try
            {
                var settingsFile = File.ReadAllText(settingsFileLocation);
                settings = JsonSerializer.Deserialize<Shared.LauncherSettings>(settingsFile) ?? settings;
            }
            catch
            {
                //ignored
            }
        }
        
        return settings;
    }
}