using System.Text.Json;

namespace Backend.Endpoints.LauncherSettings;

public class UpdateSettingsEndpoint : IEndpoint
{
    private const string Route = "/launcher-settings";
    
    public static RouteHandlerBuilder Define(IEndpointRouteBuilder app)
    {
        return app.MapPost(Route, ExecuteAsync);
    }

    private static async Task ExecuteAsync(Shared.LauncherSettings settings)
    {
        const string settingsFileLocation = "settings.json";
        var jsonOptions = new JsonSerializerOptions
        {
            WriteIndented = true
        };

        var serializedSettings = JsonSerializer.Serialize(settings, jsonOptions);
        await File.WriteAllTextAsync(settingsFileLocation, serializedSettings);
    }
}