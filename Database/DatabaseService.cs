﻿using LiteDB;

namespace Database;

public static class DatabaseService
{
    private const string DbFileLocation = "DayZLegacy.db";

    public static void Create<T>(string collectionName, BsonExpression indexPropertyExpression)
    {
        using var database = new LiteDatabase(DbFileLocation);
        if (!database.CollectionExists(collectionName))
        {
            return;
        }
        
        var collection = database.GetCollection<T>(collectionName);
        collection.EnsureIndex(indexPropertyExpression);
    }
    
    public static void Create<T>(string collectionName, BsonExpression indexPropertyExpression, T entry)
    {
        using var database = new LiteDatabase(DbFileLocation);
        var collection = database.GetCollection<T>(collectionName);
        
        collection.Insert(entry);
        collection.EnsureIndex(indexPropertyExpression);
    }
    
    public static void Create<T>(string collectionName, BsonExpression indexPropertyExpression, IEnumerable<T> entries)
    {
        using var database = new LiteDatabase(DbFileLocation);
        var collection = database.GetCollection<T>(collectionName);
        
        collection.Insert(entries);
        collection.EnsureIndex(indexPropertyExpression);
    }
    
    public static List<T> ReadAll<T>(string collectionName)
    {
        using var database = new LiteDatabase(DbFileLocation);
        if (!database.CollectionExists(collectionName))
        {
            return new List<T>();
        }
        
        var collection = database.GetCollection<T>(collectionName);
        var collectionEntries = collection.FindAll().ToList();

        return collectionEntries;
    }
    
    public static T? Read<T>(string collectionName) where T : new()
    {
        using var database = new LiteDatabase(DbFileLocation);
        var a = database.GetCollectionNames().ToArray();
        if (!database.CollectionExists(collectionName))
        {
            return new T();
        }
        
        var collection = database.GetCollection<T>(collectionName);
        var collectionEntry = collection.FindAll().ToArray();

        return collectionEntry.LastOrDefault();
    }
}