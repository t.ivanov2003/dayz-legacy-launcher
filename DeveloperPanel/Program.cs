﻿using System.Net.Http.Headers;
using Newtonsoft.Json;
using Shared;

namespace DeveloperPanel;

public static class Program
{
    public static async Task Main(string[] args)
    {
        Console.WriteLine("Enter a command");
        while (true)
        {
            string input = GetInput();
            if (input == "")
            {
                return;
            }
            
            await ExecuteCommandAsync(input);
        }
    }

    private static string GetInput(string title = "")
    {
        if (title != string.Empty)
        {
            Console.WriteLine(title);
        }
        
        return Console.ReadLine() ?? "";
    }
    
    private static async Task ExecuteCommandAsync(string input)
    {
        string[] availableCommands = ["uploadUpdate", "ls", "help"];//todo move
        string? command = availableCommands.FirstOrDefault(c => c.Equals(input, StringComparison.OrdinalIgnoreCase));
        if (command is null)
        {
            Console.WriteLine($"Command {input} not found");
            return;
        }

        if (command == "uploadUpdate")
        {
            await UploadUpdateAsync();
        }
        else if (command == "ls")
        {
            string[] dirInfo = Directory.GetDirectories(Directory.GetCurrentDirectory())
                .Concat(Directory.GetFiles(Directory.GetCurrentDirectory()))
                .ToArray();
            
            Console.WriteLine(string.Join('\n', dirInfo));
        }
        else if (command == "help")
        {
            Console.WriteLine("Available commands");
            Console.WriteLine(string.Join(',', availableCommands));
        }
    }

    private static IEnumerable<string> SplitFile(string filePath, long chunkSizeInBytes)
    {
        if (File.ReadAllBytes(filePath).Length <= chunkSizeInBytes)
        {
            return [filePath];
        }
        
        List<string> chunkPaths = new List<string>();
        byte[] buffer = new byte[chunkSizeInBytes];

        using (FileStream sourceStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
        {
            int index = 0;
            while (sourceStream.Position < sourceStream.Length)
            {
                string chunkPath = $"{filePath}.part{index}";
                using (FileStream chunkStream = new FileStream(chunkPath, FileMode.Create, FileAccess.Write))
                {
                    int bytesRead = sourceStream.Read(buffer, 0, buffer.Length);
                    chunkStream.Write(buffer, 0, bytesRead);
                }
                chunkPaths.Add(chunkPath);
                index++;
            }
        }

        return chunkPaths;
    }
    
    const string versionInfoFileName = "versionInfo.json";
    const string uploadUrl = "https://localhost:5400/uploadFiles"; //todo export
    
    private static async Task UploadUpdateAsync()
    {
        string dirPath = GetInput("Enter the directory path of the files you want to upload");
        
        //calculate version info of target folder
        //check for versionInfo.json
        //if exists compare and send only diff to UploadFilesEndpoint
        //if not create, save currentVersionInfo and send everything to UploadFilesEndpoint
        
        if (!Directory.Exists(dirPath))
        {
            Console.WriteLine($"Directory {dirPath} does not exist");
            return;
        }

        VersionInfo clientVersionInfo = new VersionInfo()
        {
            VersionNumber = new VersionNumber()
            {
                Major = 0,
                Minor = 1,
                Patch = 0
            },
            FileHashes = FilesUtility.CalculateHashes(dirPath)
        };
//todo check if server has any versions uploaded
//if so - get diffs and upload
//else - upload all


        VersionInfo? serverVersionInfo = await GetCurrentVersionHashesAsync();
        if (serverVersionInfo is not null)
        {
            string[] dirFiles = Directory.GetFiles(dirPath, "*.*", SearchOption.AllDirectories);
            
            if (serverVersionInfo.FileHashes.Count == 0)
            {
                //server has not update uploaded
                await UploadDirectoryFiles(dirFiles, dirPath);//todo remove root and get files in the method?
            }
            else
            {
                string[] newOrModifiedFiles = clientVersionInfo.FileHashes
                    .Where(h => 
                        !serverVersionInfo.FileHashes.ContainsKey(h.Key) || 
                        serverVersionInfo.FileHashes[h.Key] != h.Value)
                    .Select(h => h.Key)
                    .ToArray();
                        
                await UploadDirectoryFiles(newOrModifiedFiles, dirPath);
            }
        }

        Console.WriteLine("Uploaded files");
    }
    
    static async Task UploadDirectoryFiles(string[] filePaths, string? root = null)
    {
        using var httpClient = new HttpClient();
        Dictionary<string, string[]> fileChunksToDelete = new(); //todo this sucks remove asap
        
        foreach (var filePath in filePaths)
        {
            if (!Path.Exists(filePath))
            {
                continue;
            }

            const long maxBodySize = 52428800/5; //todo export
            string[] splitFilePaths = SplitFile(filePath, maxBodySize).ToArray();
            fileChunksToDelete.Add(filePath, splitFilePaths);
            
            foreach (string splitFilePath in splitFilePaths)
            {
                using var content = new MultipartFormDataContent();
                using var fileContent = new StreamContent(new FileStream(splitFilePath, FileMode.Open));
                fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                string? fileDirPath = null;
                if (root is not null)
                {
                    fileDirPath = filePath.Replace(root, "").Replace(Path.GetFileName(splitFilePath), "");
                }

                //if fileDirPath == Path.GetFileName(filePath)??
                content.Add(fileContent, "file",
                    fileDirPath is not null && fileDirPath != $@"\{Path.GetFileName(filePath)}" //todo simplify
                        ? Path.Join(fileDirPath, Path.GetFileName(splitFilePath))
                        : Path.GetFileName(splitFilePath));
                
                try
                {
                    var response = await httpClient.PostAsync(uploadUrl, content);
                    Console.WriteLine(response.IsSuccessStatusCode
                        ? $"File {splitFilePath} uploaded successfully"
                        : $"File {splitFilePath} failed to upload");
                }
                catch (Exception ex)
                {
                    await Console.Error.WriteLineAsync("Could not upload " + filePath + " " + ex.Message);
                }
            }
        }

        foreach (var kvp in fileChunksToDelete)
        {
            CombineFiles(kvp.Value, kvp.Key);
        }
        
        await httpClient.GetAsync("https://localhost:5400/combineFiles");
    }
    
    private static async Task<VersionInfo?> GetCurrentVersionHashesAsync()
    {
        using var httpClient = new HttpClient();
        const string getUrl = "https://localhost:5400/hashes";//todo export
        
        var currentVersionResult = await httpClient.GetAsync(getUrl);
        if (!currentVersionResult.IsSuccessStatusCode)
        {
            Console.WriteLine("Could not get current version");
            return null;
        }

        string content = await currentVersionResult.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<VersionInfo>(content);
    }
    
    private static void CombineFiles(IEnumerable<string> chunkPaths, string destinationFilePath)
    {
        using FileStream destinationStream = new FileStream(destinationFilePath, FileMode.Create, FileAccess.Write);
        foreach (string chunkPath in chunkPaths)
        {
            FileStream chunkStream = new FileStream(chunkPath, FileMode.Open, FileAccess.Read);
            chunkStream.CopyTo(destinationStream);

            chunkStream.Dispose();
            try
            {
                File.Delete(chunkPath);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Could not delete " + chunkPath + " " + ex.Message);
            }
        }
    }

/*public static class CommandUtility
{
    private static string[] _availableCommands = [];
    static CommandUtility()
    {
        FetchAvailableCommands();
    }

    private static void FetchAvailableCommands()
    {
        _availableCommands = typeof(ICommandInfo).Assembly.DefinedTypes.Where(t =>
                !t.IsInterface && t.IsAssignableTo(typeof(ICommandInfo)))
                .Select(t => (t as ICommandInfo)?.Name ?? "")
                .Where(t => t != string.Empty)
                .ToArray();
    }
}

public interface ICommandInfo
{
    string Name { get; set; }
    ICommandParameter[] Parameters { get; set; }
    Delegate? Delegate { get; set; }
}

public interface ICommandParameter
{
    string Value { get; set; }
    bool Required { get; set; }
    object? ParseValue();
}

public class IntCommandParameter : ICommandParameter
{
    public string Value { get; set; } = "";
    public bool Required { get; set; }
    public object? ParseValue()
    {
        if (int.TryParse(Value, out var value))
        {
            return value;
        }

        return null;
    }
}*/
}