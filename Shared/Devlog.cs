namespace Shared;

public class Devlog
{
    public long Id { get; set; }
    public string Title { get; set; } = "";
    public string Description { get; set; } = "";
    public DateOnly UploadDate { get; set; }
    public DevlogType Type { get; set; }
}