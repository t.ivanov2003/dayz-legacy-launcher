namespace Shared;

public static class FilesUtility
{
    public static Dictionary<string, FileHashInfo> CalculateHashes(string directory)
    {
        Dictionary<string, FileHashInfo> fileHashes = new();
        var files = Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories);
        foreach (string filePath in files)
        {
            string fileName = Path.GetFileName(filePath);
            if (!fileHashes.ContainsKey(fileName))
            {
                fileHashes[fileName] = new FileHashInfo();
            }

            fileHashes[fileName].CrcHash = CalculateCrc(filePath);
            fileHashes[fileName].SizeHash = CalculateSize(filePath);
        }

        return fileHashes;
    }

    private static uint CalculateCrc(string filePath)
    {
        uint crcValue = 0;
        byte[] buffer = new byte[1024];
        using FileStream fileStream = File.OpenRead(filePath);
        
        while (fileStream.Read(buffer, 0, buffer.Length) > 0)
        {
            crcValue = Crc32Utility.ComputeChecksum(buffer, crcValue);
        }

        return crcValue;
    }

    private static long CalculateSize(string filePath)
    {
        return new FileInfo(filePath).Length;
    }
}