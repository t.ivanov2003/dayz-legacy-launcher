namespace Shared;

public class Settings
{
    public DebugSettings DebugSettings { get; set; } = new();
    public GameSettings GameSettings { get; set; } = new();
    public LauncherSettings LauncherSettings { get; set; } = new();
}