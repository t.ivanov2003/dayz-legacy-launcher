namespace Shared;

public class LauncherSettings
{
    public string BackgroundMediaSrc { get; set; } = "img/dayz.png";
    public string BackgroundMediaType { get; set; } = "image";
}