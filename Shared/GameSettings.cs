namespace Shared;

public class GameSettings
{
    public Renderer Renderer { get; set; }
    public DisplayMode DisplayMode { get; set; }
    public Platform Platform { get; set; }
    public GameWorld World { get; set; }
    public bool NoPause { get; set; }
}