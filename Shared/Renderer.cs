﻿namespace Shared;
public enum Renderer
{
    [System.ComponentModel.DataAnnotations.Display(Name = "DirectX 9")]
    DX9 = 0,
    [System.ComponentModel.DataAnnotations.Display(Name = "DirectX 11")]
    DX11 = 1,
    Vulkan = 2
}
