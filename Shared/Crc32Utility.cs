namespace Shared;

public static class Crc32Utility
{
    private const uint Polynomial = 0xedb88320;
    private static readonly uint[] Table;

    static Crc32Utility()
    {
        Table = new uint[256];
        for (uint i = 0; i < 256; i++)
        {
            uint entry = i;
            for (int j = 0; j < 8; j++)
            {
                if ((entry & 1) == 1)
                {
                    entry = (entry >> 1) ^ Polynomial;
                }
                else
                {
                    entry >>= 1;
                }
            }
            Table[i] = entry;
        }
    }

    public static uint ComputeChecksum(byte[] bytes, uint crc32 = 0)
    {
        crc32 ^= 0xffffffff;
        foreach (byte b in bytes)
        {
            crc32 = (crc32 >> 8) ^ Table[(crc32 & 0xff) ^ b];
        }
        return crc32 ^ 0xffffffff;
    }
}