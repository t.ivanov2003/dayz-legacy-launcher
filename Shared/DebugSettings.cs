namespace Shared;

public class DebugSettings
{
    public bool RunAsAdmin { get; set; }
    public bool NoLogs { get; set; }
    public bool NoConsole { get; set; }
}