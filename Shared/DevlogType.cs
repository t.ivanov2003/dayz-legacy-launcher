namespace Shared;

public enum DevlogType
{
    Major = 1,
    Minor = 2,
    Important = 3,
    Unassigned = 0
}