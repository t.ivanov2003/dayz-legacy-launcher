namespace Shared;

public class FileHashInfo
{
    public uint CrcHash { get; set; }
    public long SizeHash { get; set; }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == GetType() && Equals((FileHashInfo)obj);
    }
    
    public override int GetHashCode()
    {
        return HashCode.Combine(CrcHash, SizeHash);
    }

    public static bool operator !=(FileHashInfo a, FileHashInfo b)
    {
        return !a.Equals(b);
    }

    public static bool operator ==(FileHashInfo a, FileHashInfo b)
    {
        return a.Equals(b);
    }
    
    private bool Equals(FileHashInfo other)
    {
        return CrcHash == other.CrcHash && SizeHash == other.SizeHash;
    }
}