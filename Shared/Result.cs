namespace Shared;

public class Result(Exception? ex)
{
    public readonly Exception? Error = ex;
    public Result() : this(null)
    {
    }
}

public class Result<T>
{
    public readonly Exception? Error;
    public readonly T? Value;
    public Result(Exception ex)
    {
        Error = ex;
    }

    public Result(T value)
    {
        Value = value;
    }
}