namespace Shared;

[AttributeUsage(System.AttributeTargets.Class | AttributeTargets.Enum)]
public class Display : Attribute
{
    public string Name { get; set; }

    public Display(string name)
    {
        Name = name;
    }
}