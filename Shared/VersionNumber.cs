namespace Shared;

public class VersionNumber
{
    public ushort Major { get; set; }
    public ushort Minor { get; set; }
    public ushort Patch { get; set; }
    
    public static bool operator >(VersionNumber a, VersionNumber b)
    {
        if (a.Major > b.Major)
        {
            return true;
        }
         
        if (a.Major == b.Major)
        {
            if (a.Minor > b.Minor)
            {
                return true;
            }
             
            if (a.Minor == b.Minor)
            {
                if (a.Patch > b.Patch)
                {
                    return true;
                }
            }
        }
        
        return false;
    }

    public static bool operator <(VersionNumber a, VersionNumber b)
    {
        if (a.Major < b.Major)
        {
            return true;
        }
         
        if (a.Major == b.Major)
        {
            if (a.Minor < b.Minor)
            {
                return true;
            }
             
            if (a.Minor == b.Minor)
            {
                if (a.Patch < b.Patch)
                {
                    return true;
                }
            }
        }
        
        return false;
    }

    public override string ToString()
    {
        return $"{Major}.{Minor}.{Patch}";
    }
}