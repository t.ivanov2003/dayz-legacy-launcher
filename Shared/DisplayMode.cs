namespace Shared;

[Display("Display mode")]
public enum DisplayMode
{
    Borderless = 0,
    Windowed = 1
}