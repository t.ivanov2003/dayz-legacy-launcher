namespace Shared;

public class VersionInfo
{
    public VersionNumber VersionNumber { get; set; } = new VersionNumber();
    public Dictionary<string, FileHashInfo> FileHashes = new();
}