namespace Shared;

public enum Platform
{
    [System.ComponentModel.DataAnnotations.Display(Name = "32-bit")]
    x86 = 0,
    [System.ComponentModel.DataAnnotations.Display(Name = "64-bit")]
    x64 = 1
}