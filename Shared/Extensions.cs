﻿namespace Shared;

public static class Extensions
{
    public static void AndForget(this Task self)
    {
        self.ContinueWith(t =>
        {
            if(t.Exception is not null)
            {
                Console.Error.WriteLine("Unhandled exception occurred while executing fire and forget task:\r\n" + t.Exception);
            }
        });
    }

    public static void AndForget(this ValueTask self)
    {
        self.AsTask().ContinueWith(t =>
        {
            if (t.Exception is not null)
            {
                Console.Error.WriteLine("Unhandled exception occurred while executing fire and forget task:\r\n" + t.Exception);
            }
        });
    }
}