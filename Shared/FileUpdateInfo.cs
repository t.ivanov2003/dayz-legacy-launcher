namespace Shared;

public class FileUpdateInfo
{
    public List<string> FilesToDelete { get; set; } = [];
    public List<string> FilesToDownload { get; set; } = [];
}