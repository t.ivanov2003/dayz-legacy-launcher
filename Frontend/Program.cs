﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using DayZLegacy.Services;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Photino.Blazor;

namespace DayZLegacy
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            var appBuilder = PhotinoBlazorAppBuilder.CreateDefault(args);
            appBuilder.RootComponents.Add<App>("app");
            
            ConfigureAppSettings(appBuilder);
            ConfigureServices(appBuilder);
            
            var app = appBuilder.Build();
            app.MainWindow.SetIconFile("./icon.ico")
                .SetTitle("DayZ Legacy")
                .SetChromeless(true)
                .SetUseOsDefaultSize(false)
                .SetUseOsDefaultLocation(false)
                .SetTop(0)
                .SetLeft(0)
                .SetWidth(960)
                .SetHeight(540)
                .Center();

            AppDomain.CurrentDomain.UnhandledException += (sender, error) =>
            {
                app.MainWindow.ShowMessage("Fatal exception", error.ExceptionObject.ToString());
            };
            
            app.Run();
        }

        static void ConfigureServices(PhotinoBlazorAppBuilder builder)
        {
            var appSettings = builder.Services.BuildServiceProvider()
                .GetRequiredService<AppSettings>();

            var serviceBaseType = typeof(IService);
            var services = serviceBaseType.Assembly.ExportedTypes
                .Where(t => t.IsClass && serviceBaseType.IsAssignableFrom(t));
            
            builder.Services.AddSingleton(_ => new HttpClient()
                {
                    BaseAddress = new Uri(appSettings.HttpClientsHost)
                })
                .AddSingleton(_ => new HubConnectionBuilder()
                    .WithUrl(appSettings.HubHost)
                    .Build());

            foreach (var service in services)
            {
                builder.Services.AddSingleton(service);
            }
        }

        static void ConfigureAppSettings(PhotinoBlazorAppBuilder builder)
        {
            var fileToDeserialize = "frontendAppSettings.json";
            if (File.Exists(fileToDeserialize))
            {
                var json = File.ReadAllText(fileToDeserialize);
                var appSettings = JsonSerializer.Deserialize<AppSettings>(json);
                builder.Services.AddSingleton(appSettings);
            }
            else
            {
                File.Create(fileToDeserialize);
                File.WriteAllText(fileToDeserialize, JsonSerializer.Serialize(new AppSettings()
                {
                    HttpClientsHost = "http://localhost:5402",
                    HubHost = "http://localhost:5402/hub"
                }));
            }
        }
    }

    class AppSettings
    {
        public string HttpClientsHost { get; init; }
        public string HubHost { get; init; }
    }
}