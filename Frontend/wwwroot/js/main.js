function applySlideDownStyle(element) {
    if(element === undefined) {
        return;
    }
    
    if(element.classList.contains('hidden')) {
        element.classList.toggle('visible');
    }
}

function changeImageBackground(src) {
    destroyVideo();
    document.body.style.backgroundImage = `url(${src})`;
}

function changeVideoBackground(src) {
    destroyImage();
    const videoWrapper = document.getElementById('video-wrapper');
    if(videoWrapper === null || videoWrapper === undefined) {
        console.log('Video wrapper is not present');
        return;
    }

    let videoElement = videoWrapper.querySelector('video');
    if(videoElement === null || videoElement === undefined) {
        videoElement = videoWrapper.appendChild(document.createElement('video'));
    }
    
    let videoSource = videoElement.querySelector('source');
    if(videoSource === null || videoSource === undefined){
        videoSource = videoElement.appendChild(document.createElement('source'));
        if(videoSource === null || videoSource === undefined) {
            console.log('Video source is not present');
            return;
        }
    }
    
    videoElement.muted = true;
    videoElement.className = "w-100 h-100 position-absolute";
    videoElement.muted = true;
    videoElement.playsinline = true;
    videoElement.autoplay = true;
    videoElement.loop = true;
    videoSource.src = src;
    videoElement.load();
}

function destroyImage() {
    document.body.backgroundImage = '';
}

function destroyVideo() {
    const videoWrapper = document.getElementById('video-wrapper');
    if(videoWrapper === null) {
        console.log('Video wrapper is not present');
        return;
    }
    
    videoWrapper.innerHTML = '';
}