using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Shared;

namespace DayZLegacy.Services;

public class LauncherService(ConfigService configService) : IService
{
    public event Action OnBackgroundChanged;

    public async Task<Result> UpdateBackgroundAsync(IBrowserFile file)
    {
        const long maxFileSizeInBytes = 1024000L * 30;
        string[] supportedVideoFormats = [".mp4", ".webm", ".mkv"];

        if (file.Size > maxFileSizeInBytes)
        {
            return new Result(
                new Exception($"File size cannot exceed {(maxFileSizeInBytes / (1000.0 * 1000.0)):F1} mb"));
        }
        
        if (string.IsNullOrWhiteSpace(file.ContentType))
        {
            return new Result( 
                    new Exception("The file you uploaded is not supported. Try with a video or an image format."));
        }
        
        try
        {
            if (file.ContentType.StartsWith("video"))
            {
                if (supportedVideoFormats.All(f =>
                        !f.Equals(Path.GetExtension(file.Name), StringComparison.InvariantCultureIgnoreCase)))
                {
                    return new Result(new Exception("The video format you uploaded is not supported. " +
                                                    "Supported formats: " + string.Join(", ", supportedVideoFormats)));
                }
            }
            else if (!file.ContentType.StartsWith("image"))
            {
                return new Result(
                    new Exception( "The file you uploaded is not supported. Try with a video or an image format."));
            }

            await using var stream = file.OpenReadStream(maxFileSizeInBytes);
            using var memoryStream = new MemoryStream();
            await stream.CopyToAsync(memoryStream);

            var fileName = file.Name.Replace(" ", "");
            await File.WriteAllBytesAsync(Path.Join("wwwroot", fileName), memoryStream.ToArray());

            configService.Settings.LauncherSettings.BackgroundMediaType = file.ContentType;
            configService.Settings.LauncherSettings.BackgroundMediaSrc = fileName;
            configService.SaveConfig();
            OnBackgroundChanged?.Invoke();

            return new Result();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception while reading file " + ex);
            return new Result(ex);
        }
    }
}