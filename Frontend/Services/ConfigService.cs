using System;
using System.IO;
using System.Text.Json;
using Shared;

namespace DayZLegacy.Services;
public class ConfigService : IService
{
    public Settings Settings { get; private set; } = new();
    private readonly string _configPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}/DayZLegacy/settings.json";
    private readonly JsonSerializerOptions _jsonSerializerOptions = new()
    {
        WriteIndented = true
    };
    
    public ConfigService()
    {
        LoadConfig();
    }

    private void LoadConfig()
    {
        if (!File.Exists(_configPath))
        {
            try
            {
                Directory.CreateDirectory(_configPath[.._configPath.LastIndexOf('/')]);
                File.WriteAllText(_configPath, JsonSerializer.Serialize(Settings, _jsonSerializerOptions));
            }
            catch(Exception ex)
            {
                Console.WriteLine("Could not create config file with default settings" + ex);
            }
            
            return;
        }

        try
        {
            var settingsFile = File.ReadAllText(_configPath);
            Settings = JsonSerializer.Deserialize<Settings>(settingsFile);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Could not load configuration " + ex);
        }
    }
    
    public void SaveConfig()
    {
        if (!File.Exists(_configPath))
        {
            Console.WriteLine("Config file does not exist");
            return;
        }

        try
        {
            File.WriteAllText(_configPath, JsonSerializer.Serialize(Settings, _jsonSerializerOptions));
        }
        catch (Exception ex)
        {
            Console.WriteLine("Could not load configuration " + ex);
        }
    }
}