using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Shared;

namespace DayZLegacy.Services;

public class DevlogService : IService, IDisposable
{
    private readonly HttpClient _httpClient;
    
    public DevlogService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    
    public async Task<Devlog[]> GetDevlogsAsync()
    {
        return await _httpClient.GetFromJsonAsync<Devlog[]>("/devlogs");
    }

    public void Dispose()
    {
        _httpClient?.Dispose();
    }
}