using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Shared;

namespace DayZLegacy.Services;

public class PatchService(ConfigService configService, HttpClient client) : IService
{
    private readonly string[] _dayZExeFileNames = ["DayZ_x86.exe", "DayZ_x64.exe"];
    public event Action? OnNeedsUpdate;
    private bool _needsUpdate;

    public bool NeedsUpdate
    {
        get => _needsUpdate;
        private set
        {
            _needsUpdate = value;
            if (_needsUpdate)
            {
                OnNeedsUpdate?.Invoke();
            }
        }
    }

    public Result HasValidExePath()
    {
        var directoryFiles = Directory.GetFiles(Directory.GetCurrentDirectory());   
        if (directoryFiles.Any(fileName => _dayZExeFileNames.Any(exe => exe == fileName)))
        {
            const string steamApiFilename = "steam_api64.dll";
            if (directoryFiles.Any(fileName => fileName == steamApiFilename))
            {
                return new Result();
            }

            return new Result(
                new FileNotFoundException($"{steamApiFilename} doesn't exist in the exe directory."));
        }

        return new Result(new FileNotFoundException());
    }

    public Result StartGame()
    {
        try
        {
            Process.Start(GetGameStartProcess());
            return new Result();
        }
        catch (Exception ex)
        {
            return new Result(ex);
        }
    }

    public async Task CheckForUpdateAsync()
    {
        //todo add listen to ws
        var hashes = FilesUtility.CalculateHashes(Directory.GetCurrentDirectory());
        var response = await client.PostAsJsonAsync("/updateCheck", hashes);
        if (response.IsSuccessStatusCode)
        {
            var content = await response.Content.ReadAsStringAsync();
            NeedsUpdate = JsonSerializer.Deserialize<bool>(content);
        }
    }
    
    public async Task<Result> PatchFilesAsync()
    {
        var hashes = FilesUtility.CalculateHashes(Directory.GetCurrentDirectory());
        try
        {
            var response = await client.PostAsJsonAsync("/patchFiles", hashes);
            if (response.IsSuccessStatusCode)
            {
                var filesUpdateInfoResult = await response.Content.ReadFromJsonAsync<Result<FileUpdateInfo>>();
                var fileDownloadResponse = await client.PostAsJsonAsync("/downloadUpdate", JsonSerializer.Serialize(filesUpdateInfoResult));
                if (fileDownloadResponse.IsSuccessStatusCode)
                {
                    var contentDisposition = fileDownloadResponse.Content.Headers.ContentDisposition;
                    var contentStream = await response.Content.ReadAsStringAsync();
                    if (contentDisposition?.FileName is null)
                    {
                        return new Result(
                            new Exception("Could not get patch files from the server response"));
                    }
                    
                    await File.WriteAllTextAsync(contentDisposition.FileName, contentStream);
                }
                else
                {
                    return new Result(
                        new Exception("Could not get patch files response from the server " + response.ReasonPhrase));
                }
            }
            else
            {
                return new Result(
                    new Exception("Could not send client file info to the server " + response.ReasonPhrase));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error patching game files + " + ex.Message);
            return new Result(ex);
        }

        return new Result();
    }

    private ProcessStartInfo GetGameStartProcess()
    {
        List<string> arguments = [];
        if (configService.Settings.DebugSettings.NoConsole)
        {
            arguments.Add("-noConsole");
        }
        if (configService.Settings.DebugSettings.NoLogs)
        {
            arguments.Add("-noLogs");
        }
        if (configService.Settings.GameSettings.DisplayMode == DisplayMode.Windowed)
        {
            arguments.Add("-window");
        }
        if (configService.Settings.GameSettings.NoPause)
        {
            arguments.Add("-noPause");
        }
        
        arguments.Add($"-{configService.Settings.GameSettings.Renderer.ToString().ToLower()}");
        arguments.Add($"-world={configService.Settings.GameSettings.World}");

        string gameExeFileName = "";
        switch (configService.Settings.GameSettings.Platform)
        {
            case Platform.x86:
                gameExeFileName = "DayZ_x86.exe";
                break;
            case Platform.x64:
                gameExeFileName = "DayZ_x64.exe";
                break;
        }
        
        ProcessStartInfo processInfo = new(gameExeFileName, arguments);
        if (configService.Settings.DebugSettings.RunAsAdmin)
        {
            processInfo.UseShellExecute = true;
            processInfo.Verb = "runas";
        }
        return processInfo;
    }
}