using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Shared;

namespace DayZLegacy.Services;

public class ConnectionService : IService, IAsyncDisposable
{
    public event Action? OnConnectionChanged;

    public bool IsConnected
    {
        get => _isConnected;
        private set
        {
            if (value == _isConnected)
            {
                return;
            }

            _isConnected = value;
            OnConnectionChanged?.Invoke();
        }
    }

    private const int ConnectionCheckInterval = 1000;
    private readonly HubConnection _connection;
    private bool _isConnected;
    
    public ConnectionService(HubConnection connection)
    {
        _connection = connection;
        _connection.Closed += HandleConnectionClosedAsync;
        _connection.Reconnected += HandleReconnectedAsync;

        StartAsync().AndForget();
    }

    private Task HandleReconnectedAsync(string arg)
    {
        IsConnected = true;
        
        RunAsync().AndForget();
        return Task.CompletedTask;
    }

    private Task HandleConnectionClosedAsync(Exception ex)
    {
        IsConnected = false;
        Console.WriteLine("Disconnected: " + ex.Message);
        
        StartAsync().AndForget();
        return Task.CompletedTask;
    }

    private async Task StartAsync()
    {
        try
        {
            await _connection.StartAsync();
            IsConnected = true;

            RunAsync().AndForget();
        }
        catch (Exception ex)
        {
            HandleConnectionClosedAsync(ex).AndForget();
        }
    }

    private async Task RunAsync()
    {
        while (IsConnected)
        {
            try
            {
                await _connection.InvokeAsync("ConnectionCheck");
                await Task.Delay(ConnectionCheckInterval);
            }
            catch (Exception e)
            {
                HandleConnectionClosedAsync(e).AndForget();
            }
        }
    }

    public async ValueTask DisposeAsync()
    {
        _connection.Closed -= HandleConnectionClosedAsync;
        _connection.Reconnected -= HandleReconnectedAsync;

        if (_connection is not null)
        {
            await _connection.StopAsync();
            await _connection.DisposeAsync();
        }
    }
}