using System;
using System.Threading.Tasks;
using Shared;

namespace DayZLegacy.Services;

public class AlertService : IService
{
    public Alert? Alert { get; private set; }
    private const int HideDelayInMs = 3000;
    public event Action OnAlert;
    
    public void AddAlert(AlertType type, string message)
    {
        Alert = new Alert()
        {
            Type = type,
            Message = message
        };
        
        OnAlert?.Invoke();
        HideAsync().AndForget();
    }
    
    public void AddInformation(string message)
    {
        Alert = new Alert()
        {
            Type = AlertType.Information,
            Message = message
        };
        
        OnAlert?.Invoke();
        HideAsync().AndForget();
    }
    
    public void AddError(string message)
    {
        Alert = new Alert()
        {
            Type = AlertType.Error,
            Message = message
        };
        
        OnAlert?.Invoke();
        Console.WriteLine("shown");
        HideAsync().AndForget();
    }

    public async Task HideAsync(bool forceHide = false)
    {
        if (!forceHide)
        {
            await Task.Delay(HideDelayInMs);
        }

        Alert = null;
        OnAlert?.Invoke();
        Console.WriteLine("hidden");
    }
}

public class Alert
{
    public AlertType Type { get; set; }
    public string Message { get; set; } = "";
}

public enum AlertType
{
    Information,
    Error
}